[[_TOC_]]

# Week 15: Web Vulnerabilities and Hardening


## Part 1: Q&A

#### The URL Cruise Missile

The URL is the gateway to the web, providing the user with unrestricted access to all available online resources. In the wrong hands can be used as a weapon to launch attacks.

Use the graphic below to answer the following questions:

| Protocol         | Host Name                 | Path                   | Parameters               |
| ---------------- | :-----------------------: | ---------------------- | ------------------------ |
| http://      | `www.buyitnow.tv`     | /add.asp           | ?item=price#1999     |


1. Which part of the URL can be manipulated by an attacker to exploit a vulnerable back-end database system
   - **Answer:** The part of the URL that can be manipulated by an attacker to exploit a vulnerable database system are the parameters included; in this case, `item=price#1999`. If an application allows for the parameters to be set and modified in the URL, and does not protect against these modifications, an attacker can tamper with the values of the attributes directly in the database.

2. Which part of the URL can be manipulated by an attacker to cause a vulnerable web server to dump the `/etc/passwd` file? Also, name the attack used to exploit this vulnerability.
   - **Answer:** A path traversal attack is when an attacker tries to "back track" deeper into the file system than the default web root path by inserting malicious characters into the variable parameter. 


3. Name three threat agents that can pose a risk to your organization.
   - **Answers:**
   - *State-sponsored hackers* are threat agents that go after specific targets like certain types of industries and infrastructure (water, energy, politics, health care, etc) with the goal of disrupting operations thus affecting the CIA of the targeted victim.
   - *Organized criminal groups* are threat agents that target victims in small to large industries. These groups have the financials means, "hacking" expertise, and have access to malicious software that aims to attack victims that operate one or more vulnerable software like Microsoft Windows OS, Windows-based applications, and various open source programs (e.g. WordPress, Drupal, Apache). The WannaCry ransomware worm posed a big threat to the healthcare industry in 2017 to 2018 as it was infecting Windows systems, encrypting important files and extorting businesses to pay a ransom in order to access their files again.
   - *Employees and contractors* at companies, small and large, remain to be one of the biggest threats to an organization as they have inside access to systems containing sensitive data. Either intentional (stealing company secrets) or unintentional (misconfigured service), people are usually at the root of the problem when it comes to increasing the attack surface that threat agents can penetrate.


4. What kinds of sources can act as an attack vector for injection attacks?
   - **Answer:** The most common method of injection attacks is when there is untrusted data entered into web applications which is not sanitized or validated before the application executes the malicious data, usually in the form of code (e.g. SQL, noSQL, LDAP, OS commands). This attack vector is the number one security risk according to [OWASP](https://owasp.org/www-project-top-ten/). Depending on the application, the source of the injection attack can come from a OS command entered in a form field that's not sanitized, a SQL or programming (PHP, ASP) command entered in the URL and parameters, or from malicious scripts injected into trusted websites.


5. Injection attacks exploit which part of the CIA triad?
   - **Answers:**
   - Injection attacks impact all three principles of the CIA triad in various ways and in different phases of the attack lifecycle. The main principle impacted by injection attacks is the confidentiality of data. Sensitive data is meant to be contained only for restricted use and personnel. Hackers pose a threat to the confidentiality of data by exploiting vulnerabilities with the intention of accessing unauthorized parts of the application or system.
   - The integrity principle of the CIA triad is also impacted as hackers who have unauthorized access to sensitive data can change the data set like prices, quantity, and other values. Even worse, hackers can completely delete your data causing a major impact to your business.
   - If a hacker uses an injection attack to successfully exploit your web application, not only is your sensitive data at risk of theft, corruption, or alteration, it also renders your application unavailable or with limited functionality.


6. Which two mitigation methods can be used to thwart injection attacks?
   - **Answers:**
   - The two mitigation methods that can be used to thwart injection attacks include sanitizing any submitted data into your web application. Once sanitized, the data should be validated to ensure it contains proper data and that it's formed correctly. This practice prevents dangerous characters entered, or malformed requests, are stripped away and validated good before being passed onto middleware or a database backend. 
   - Another method used to thwart injection attacks is updating applications to versions that are not susceptible to exploitable vulnerabilities.

---
### Web Server Infrastructure

Web application infrastructure includes sub-components and external applications that provide efficiency, scalability, reliability, robustness, and most critically, security.

- The same advancements made in web applications that provide users these conveniences are the same components that criminal hackers use to exploit them. Prudent security administrators need to be aware of how to harden such systems.


Use the graphic below to answer the following questions:

| Stage 1        | Stage 2             | Stage 3                 | Stage 4              | Stage 5          |
| :------------: | :-----------------: | :---------------------: | :------------------: | :--------------: |
| **Client**     | **Firewall**        | **Web Server**          | **Web Application**  | **Database**     |
   
   
1. What stage is the most inner part of the web architecture where data such as, customer names, addresses, account numbers, and credit card info, is stored?
   - **Answer:** The most inner part of the web architecture depicted in the example above is the *database*, where sensitive data like customer names, addresses, account numbers, and credit card info are stored.


2. Which stage includes online forms, word processors, shopping carts, video and photo editing, spreadsheets, file scanning, file conversion, and email programs such as Gmail, Yahoo and AOL.
   - **Answer:** The *web application* is the stage that provides all the mentioned resources that customers access through the web server.


3. What stage is the component that stores files (e.g. HTML documents, images, CSS stylesheets, and JavaScript files) that's connected to the Internet and provides support for physical data interactions between other devices connected to the web?
   - **Answer:** The *web server* is the stage that provides all the mentioned resources that customers access.


4. What stage is where the end user interacts with the World Wide Web through the use of a web browser?
   - **Answer:** The *client* is where the end user interacts with the World Wide Web using a web browser.


5. Which stage is designed to prevent unauthorized access to and from protected web server resources?
   - **Answer:** The *firewall* is designed to prevent unauthorized access to and from protected web server resources.

----

### Server Side Attacks

In today’s globally connected cyber community, network and OS level attacks are well defended through the proper deployment of technical security controls such as, firewalls, IDS, Data Loss Prevention, EndPoint and security. However, web servers are accessible from anywhere on the web, making them vulnerable to attack.


1. What is the process called that cleans and scrubs user input in order to prevent it from exploiting security holes by proactively modifying user input.
   - **Answer:** Input sanitization is the process that cleans and scrubs user input (e.g. special metacharaters like single quotes) in order to prevent it from exploiting security holes by proactively modifying user input.


2. Name the process that tests user and application-supplied input. The process is designed to prevent malformed data from entering a data information system by verifying user input meets a specific set of criteria (i.e. a string that does not contain standalone single quotation marks).
   - **Answer:**  Input validation is the process of preventing malformed data from entering a data information system. This practice prevents dangerous characters entered, or malformed requests, are stripped away and validated good before being passed onto middleware or a database backend.


3. **Secure SDLC** is the process of ensuring security is built into web applications throughout the entire software development life cycle. Name three reasons why organization might fail at producing secure web applications.

**Answers:** Three reasons why organizations fail at producing secure web applications include, a. the high implementation costs of the process, and maybe the software, involved in securing web apps, b. insufficient support from stakeholders in turn causing lack of interest and limited time spent on Secure SDLC practices, and c. lack of incentive and motivation to change or modernize their existing SDLC work flow and processess. 


1. How might an attacker exploit the `robots.txt` file on a web server?

**Answer:** Using the contents (i.e. what crawlers use to see what they can and can't access of the website) of robots.txt, hackers can disregard the "restricted access" aspects and gain more information about sensitive areas and proceed to attempt exploiting a vulnerability in the web server or application.


1. What steps can an organization take to obscure or obfuscate their contact information on domain registry web sites?

**Answer:** An organization can use a domain privacy service usually offered by all registrars to obscure or obfuscate the contact information of a domain owner.

  
6. True or False: As a network defender, `Client-Side` validation is preferred over `Server-Side` validation because it's easier to defend against attacks.

   - Explain why you chose the answer that you did.

**Answer:** False; client-side validation is **not** preferred over server-side validation because it is easier to bypass client-side validation routines in scripting (i.e. javascript) in turn leaving your users to input dangerouse characters into your web server, application and backend database. A network defender would rather the server validate the user input as a final line of defense against maliscous code.

____

### Web Application Firewalls

WAFs are designed to defend against different types of HTTP attacks and various query types such as SQLi and XSS.

WAFs are typically present on web sites that use strict transport security mechanisms such as online banking or e-commerce websites.


1. Which layer of the OSI model do WAFs operate at?

**Answer:** WAFs operate at layer 7 of the OSI model and as a byproduct of the strengths of WAF products, also help with minimizing synfloods occuring at the network layer(4) and protect TLS connections at the transport layer (5)


1. A WAF helps protect web applications by filtering and monitoring what?

**Answer:**  A WAF helps protect web applications by filtering and monitoring HTTP/S requests for malicious traffic.


3. True or False: A WAF based on the negative security model (Blacklisting) protects against known attacks, and a WAF based on the positive security model (Whitelisting) allows pre-approved traffic to pass.

**Answer:** True
____

### Authentication and Access Controls

Security enhancements designed to require users to present two or more pieces of evidence or credentials when logging into an account is called multi-factor authentication.

- Legislation and regulations such as The Payment Card Industry (PCI) Data Security Standard requires the use of MFAs for all network access to a Card Data Environment (CDE).

- Security administrators should have a comprehensive understanding of the basic underlying principles of how MFA works.


1. Define all four factors of multifactor authentication and give examples of each:

   - Factor 1: Standard login inputs like passwords, PINs or questions that only a human to respond to.
   
   - Factor 2: Physical keys like smart cards or hardware or software-based tokens.
   
   - Factor 3: Biometrics like retinal scans, fingerprints or hand or face scans.
   
   - Factor 4: Location-based access using GPS or automated phone calls to home locations.


2. True or False: A password and pin is an example of 2-factor authentication.

**Answer:** False. A password and PIN is an example of only something you know. 2-factor authentication is missing as there is no factor of a something you have like a hardware-based, or virtual, token or retina or fingerprint scan.

  
3. True or False: A password and `google authenticator app` is an example of 2-factor authentication.

**Answer:** True. A password and `google authenticator app` is an example of 2-factor authentication as a password is something you know and a software-based app on a phone or computer is something you have.

  
4. What is a constrained user interface? 

**Answer:** A constrained user interface is a mitigation strategy that restricts what a user can do based on their privileges.

----
____

## Part 2: The Challenge 

**Did not finish the challenges**

---
