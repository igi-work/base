[[_TOC_]]
# Week 14: Web Development

## Overview

Reviewing many of the concepts and tools covered in the Web Development unit. 

---

## HTTP Requests and Responses

Answer the following questions about the HTTP request and response process.

1. What type of architecture does the HTTP request and response process occur in?
- The HTTP request and response process occurs in a client-server model at the application layer (layer 7). The clients are the requesters of resources and the servers providing said resources are the providers. 

2. What are the different parts of an HTTP request? 
- The various parts of an HTTP request include:
  - the **client** in the form of a web browser on a phone or a computer used to request some resource
  - the **request** comprised of protocol-specific attributes within the HTTP headers (e.g. method of request, timestamp, client and content type, ur,)
  - the **server** fulfilling the request made by the client that provides the requested resource 
  - the **response** that the server sends the client (static or dynamicly-created images and text, data from database or other centralized resources)

3. Which part of an HTTP request is optional?
   - The part of an HTTP request that is optional is the request body like in a GET request that's made for a host with no query parameters provided in the URL or in the body of the request.  

4. What are the three parts of an HTTP response?
- The three parts of an HTTP response include:
  - the **status line** with information on the status of the response (a code and a message; good, bad, error in network or on the server side)
  - **HTTP headers** that contain information about the response to the client's resource request. When the resource is available, the HTTP header fields can contain information like cookie bits, content type and server info). Another example of information in HTTP headers has to do when authentication is complete the request-response transaction. 
  - **the body** contains the data that the requestor originally intended to consume *if* the server has the data being requested (represented by status codes in the 2 hundreds (2xx)). If the server does not have the data, the body in the response will be empty along with the status code representing errors. 

5. Which number class of status codes represents errors?
   - The number class of status codes that represent errors is the **4 hundreds (4XX)**. This class of codes mostly deals with client-side errors, although, the classic 404 error deals with a resource not being found on the server. This too can be considered a client-side error as the incorrect choice was made. :-) Whether an error in this class deals with a bad request in the URL or a user not having enough permissions, 4XX codes deals with incorrect requests made from the client that are permanently "failed".

6. What are the two most common request methods that a security professional will encounter?
   - The two most common request methods that a security professional will encounter include **GET** requests requesting data from a server and **POST** requests providing data to a server.

7. Which type of HTTP request method is used for sending data?
   - The type of HTTP request method that's used for sending data is the **POST request method.** One example of this method is like when a person "posts" valid login credentials to the server in order to access private resources.

8. Which part of an HTTP request contains the data being sent to the server?
   - The part of an HTTP request that contains the data being sent to the server is the body which contains the payload to the server, like the content of an HTML form. 

9.  In which part of an HTTP response does the browser receive the web code to generate and style a web page?
10. - The part of an HTTP response that the browser receives the web code to generate and style a web page is in the response body as style and substance is both considered part of the payload that the requestor is looking for from the server.

## Using curl

Answer the following questions about `curl`:

11. What are the advantages of using `curl` over the browser?
    - The  advantages of using `curl` over the browser is that it is a command line client that can upload and download data quickly and without the need of a GUI-based browser. Curl is faster and safer than browsers as there is no presentation code (html , css) is executed so the processing time is faster and the risk of your client (browser) executing some malicious code is smaller as a command line client does not need a "pretty" UI to upload and download data. Curl the command line tool and the C library is portable to many different environments and platforms so benefits from having a log user base and use case. The number of supported protocols and options makes curl advantageous to use for every day use and in scripted and scheduled fashion.

12. Which `curl` option is used to change the request method?
    - The `-X` option is used to change the request method. The default request method is GET when working with HTTP; -X changes this for HTTP and other supported protocols.

13. Which `curl` option is used to set request headers?
    - The `-H` options is used to set custom request headers that will, a. be added to the usual header metadata that curl adds, or, 2. overwrite any existing header values that are created by default.

14. Which `curl` option is used to view the response header?
    - The `-i` option is used to view the response header *and* body (i.e. html, css). The `-I` option returns only the headers.

15. Which request method might an attacker use to figure out which HTTP requests an HTTP server will accept?
    - The request method that an attacker can use to figure out which HTTP requests a web server can accept is **OPTIONS** as this is used to see the available HTTP methods on a web server.

## Sessions and Cookies

Recall that HTTP servers need to be able to recognize clients from one another. They do this through sessions and cookies.

Answer the following questions about sessions and cookies:

16. Which response header sends a cookie to the client?

    ```HTTP
    HTTP/1.1 200 OK
    Content-type: text/html
    Set-Cookie: cart=Bob
    ```
    - The **`Set-Cookie: cart=Bob`** line sends a cookie the client.

17. Which request header will continue the client's session?

    ```HTTP
    GET /cart HTTP/1.1
    Host: www.example.org
    Cookie: cart=Bob
    ```
    - The **`Cookie: cart=Bob`** line will continue the clients session.

## Example HTTP Requests and Responses

Look through the following example HTTP request and response and answer the following questions:

**HTTP Request**

```HTTP
POST /login.php HTTP/1.1
Host: example.com
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 34
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36

username=Barbara&password=password
```

18. What is the request method?
    - The request method is **POST**.

19. Which header expresses the client's preference for an encrypted response?
    - The header that expresses the client's preference for an encrypted response is **`Accept-Encoding: gzip, deflate, br`**. In this case, the web server is advertising that `gzip, deflate, br` are support. The server selects the one to use then responds to the client with the `Content-Encoding` response header.

20. Does the request have a user session associated with it?
    - The request does have a session associated with the server because the request body contains login credentials that are used to authenticate with the server.

21. What kind of data is being sent from this request body?
    - The data in the request body is plain-text **basic authentication**, hopefully, transmitted over HTTPS. 

**HTTP Response**

```HTTP
HTTP/1.1 200 OK
Date: Mon, 16 Mar 2020 17:05:43 GMT
Last-Modified: Sat, 01 Feb 2020 00:00:00 GMT
Content-Encoding: gzip
Expires: Fri, 01 May 2020 00:00:00 GMT
Server: Apache
Set-Cookie: SessionID=5
Content-Type: text/html; charset=UTF-8
Strict-Transport-Security: max-age=31536000; includeSubDomains
X-Content-Type: NoSniff
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block

[page content]
```

22.  What is the response status code?
   - The response status code is **200**.

23.  What web server is handling this HTTP response?
   - The **Apache** web server is handling this HTTP response.

24.  Does this response have a user session associated to it?
   - This response does have a session associated with it as the **`Set-Cookie`** header has a cookie name and value associated with the session.

25.  What kind of content is likely to be in the [page content] response body?
   - The page content (payload) is likely static text (html encoded with UTF-8) in the response body based on the header and value, **`Content-Type: text/html; charset=UTF-8`**. 

26.  If your class covered security headers, what security request headers have been included?
   - The security request headers included in the example and in the class material is **`Strict-Transport-Security: max-age=31536000; includeSubDomains`**. *"This headers informs the browser that it should never load a site using HTTP and should automatically convert all attempts to access the site using HTTP to HTTPS requests instead"* [reference](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type)

## Monoliths and Microservices

Answer the following questions about monoliths and microservices:

27. What are the individual components of microservices called?
    - The individual components of microservices are called **services** and each service is configured to perform one function like read and respond to HTTP requests, read from and write to a database, or parse and sanitize data taken from a web server and deliver to a database server.

28. What is a service that writes to a database and communicates to other services?
    - A **back-end server** is a service that writes to a database server and processes data received from a front-end web server. The back-end services acts like a middle man between the web server and the database server.

29. What type of underlying technology allows for microservices to become scalable and have redundancy?
    - **Containers** are an underlying technology that allows for microservices to become scalable and have redundancy as they run as isolated virtual operating systems that only use and link in the minimal number of OS resources. If a service needed to be scaled out, one can use docker and docker tools that would enable you to deploy multiple copies of a service.

## Deploying and Testing a Container Set

Answer the following questions about multi-container deployment:

30. What tool can be used to deploy multiple containers at once?
    - **Docker compose** is a tool that can be used to deploy multiple containers at once.

31. What kind of file format is required for us to deploy a container set?
    - Docker uses the **YAML** file format to deploy one or more containers. 

## Databases

32. Which type of SQL query would we use to see all of the information within a table called `customers`?
    - We would use a `SELECT` query to see all the information within a table called `customers`. 
    - `SELECT * FROM customers;`

33. Which type of SQL query would we use to enter new data into a table? (You don't need a full query, just the first part of the statement.)
    - We would use a `INSERT INTO` quer to enter new data into a table.
    - `INSERT INTO ``customers`` (column_1, column_2,...) VALUES (value_1,value_2,...);`

34. Why would we never run `DELETE FROM <table-name>;` by itself?
    - You would never run `DELETE FROM <table-name>;` by itself as it's a desctructive command that deletes everything from the table.

