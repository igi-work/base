[[_TOC_]]

# Unit 16: Penetration Testing - part 1

## Google Dorking
Google *dorking* is when you used advanced internet search terms and features in either Google or other search engines to investigate information from websites that otherwise would not be intentionally available.

**Answer:** Using Google and the search, `site:CEO@demo.testfire.net`, I identified that the Chief Executive Officer of Altoro Mutual is Karl Fitsgerald (aka Karl Fitzgerald).

- How can this information be helpful to an attacker?
  - **Answer:** Uncovering information that would otherwise go unnoticed is helpful to an attacker because this, along with other publicly-available information, can lead the attacker collecting significant personal information about where the CEO lives, his family members and other details like email addresses and social media accounts. This information can lead to targetting the CEO with specially-crafted emails or malicious payloads.

## Step 2: DNS and Domain Discovery

Enter the IP address for `demo.testfire.net` into Domain Dossier and answer the following questions based on the results:

  1. Where is the company located: **Dallas, TX**

  2. What is the NetRange IP address: **65.61.137.64/26**

  3. What is the company they use to store their infrastructure: **Rackspace**

  4. What is the IP address of the DNS server: **65.61.137.117**

## Step 3: Shodan

- What open ports and running services did Shodan find?
  - **Answer:** The open ports that Shodan found are 80, 443, and 8080. The running service found on those ports includes Apache Tomcat / Coyote JSP engine

## Step 4: Recon-ng

- Install the Recon module `xssed`. 
- Set the source to `demo.testfire.net`. 
- Run the module. 

### Is Altoro Mutual vulnerable to XSS? 

**Answer:** Yes, the Altoro Mutual website is vulnerable to cross-site scripting attacks. This was determined using Recon-ng and the `xssed` module.

## Step 5: Zenmap

Your client has asked that you help identify any vulnerabilities with their file-sharing server. Using the Metasploitable machine to act as your client's server, complete the following:

- Command for Zenmap to run a service scan against the Metasploitable machine:
  - `nmap -T4 -A -v 192.168.0.10`
 
- Bonus command to output results into a new text file named `zenmapscan.txt`:
  - `nmap -T4 -A -v 192.168.0.10 > zenmapscan.txt`

- Zenmap vulnerability script command: 
  - `nmap -Pn --script vuln -sv 192.168.0.10`

- Once you have identified this vulnerability, answer the following questions for your client:
  1. What is the vulnerability:
     1. The vulnerabilities found on the client's server include:
        1. port 21: vsFTPd version 2.3.4 backdoor (CVE:CVE-2011-2523)
        2. port 1099: java-rmi is found to be vulnerable
        3. port 5432: PostgreSQL DB 8.3.0-7 (CVE-2014-0224)
        4. SSL versions used by multiple programs (SSL POODLE: CVE-2014-3566)
        5. port 8180: Slowloris DOS attack (CVE-2007-6750)

  2. Why is it dangerous?
     1. **Answer:** It is dangerous to have so many vulnerabilities exposed because it increases the attack surface that a hacker has to exploit one or more weaknesses.

  1. What mitigation strategies can you recommendations for the client to protect their server:
     1. **Answer:** The mitigation strategies that I would recommend to the client to protect their server includes, a. proactively updating and patching the software on the system to prevent attacks, b. deploying and operating an up-to-date endpoint protection software to prevent attackers from actively exploiting the system, c. maintaining a centralized logging and auditing software on, or away from, the server to gain insight into malicious activity happening, and d. strengthening the cybersecurity knowledge to improve awareness and prevention in the cybersecurity field.



