#!/bin/bash 
#
# Week 4 Homework - Linux System Administration
# 

scratch=${HOME}/SCRATCH

###############################################################################
# Step 1: Ensure/Double Check Permissions on Sensitive Files
###############################################################################
 
# 1. Permissions on /etc/shadow should allow only root read and write access.
# - Command to inspect permissions:
# - Command to set permissions (if needed):

# 2. Permissions on /etc/gshadow should allow only root read and write access.
# - Command to inspect permissions:
# - Command to set permissions (if needed):

# 3. Permissions on /etc/group should allow root read and write access, and allow everyone else read access only.
# - Command to inspect permissions:
# - Command to set permissions (if needed):

# 4. Permissions on /etc/passwd should allow root read and write access, and allow everyone else read access only.
# - Command to inspect permissions:
# - Command to set permissions (if needed):
###############################################################################
#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# Two arrays containing files
#protectedFiles=( "/root/SCRATCH/shadow"  "/root/SCRATCH/gshadow" )
#unprotectedFiles=( "/root/SCRATCH/passwd" "/root/SCRATCH/group" )
protectedFiles=( "/etc/shadow"  "/etc/gshadow" )
unprotectedFiles=( "/etc/passwd" "/etc/group" )

# Octal perms that equal rw for root and nothing else for everyone else
protectedPerms=600

# Octal perms that equal rw for root and nothing else for everyone else
unprotectedPerms=644

# Owner ID (root)
goodOwnerID=0

function step_1 ( ) {
    #------------------------------------------------------------------------------
    
    echo "++++++++++++++++++++++++ STEP 1 +++++++++++++++++++++++++++++++++"
    # For each protectedFile, set permissions
    for eachFile1 in "${protectedFiles[@]}"
    do
    	# Get owner ID
    	ownerID=$( stat -c %u $eachFile1 )

    	# Get octal perms 
    	perms=$( stat -c %a $eachFile1 )

    	# Check permissions are good and owner is root
    	if [[ "$perms" -ne "$protectedPerms" ]] || [[ "$ownerID" -ne "$goodOwnerID" ]]; then
    		echo -e "\nNo! $eachFile1 has the wrong perms of ${perms} and / or the wrong ownership of ${ownerID}. Setting them correctly"
	
    		# Set the perms to known good state
    		chmod -v $protectedPerms $eachFile1

    		# Set the ownership to known good state
    		chown -v root:root $eachFile1
    	else
    		# eachFile1 is good.....
    		echo -e "\nYay! $eachFile1 has the correct perms of ${perms} and is owned by root with ID, ${ownerID}"
    	fi 
    done

    #------------------------------------------------------------------------------
    # For each unprotectedFile, set permissions
    for eachFile2 in "${unprotectedFiles[@]}"
    do
    	# Get owner ID
    	ownerID=$( stat -c %u $eachFile2 )

    	# Get octal perms 
    	perms=$( stat -c %a $eachFile2 )

    	# Check permissions are good and owner is root
    	if [[ "$perms" -ne "$unprotectedPerms" ]] || [[ "$ownerID" -ne "$goodOwnerID" ]]; then
    		echo -e "\nNo! $eachFile2 has the wrong perms of ${perms} and / or the wrong ownership of ${ownerID}. Setting them correctly"
	
    		# Set the perms to known good state
    		chmod -v $unprotectedPerms $eachFile2

    		# Set the ownership to known good state
    		chown -v root:root $eachFile2
    	else
    		# eachFile2 is good.....
    		echo -e "\nYay! $eachFile2 has the correct perms of ${perms} and is owned by root with ID, ${ownerID}"
    	fi 
    done
}

###############################################################################
# Step 2: Create User Accounts
###############################################################################

# 1. Add user accounts for sam , joe , amy , sara , and admin .
# - Command to add each user account (include all five users):

# 2. Ensure that only the admin has general sudo access.
# - Command to add admin to the sudo group:
###############################################################################
#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# User array
userArray1=( "admin" "joe" "amy" "sara" "sam" )

# Files to process
sudoers=/etc/sudoers
passwd=/etc/passwd
group=/etc/group

#importantFiles=()

function step_2 () {
    #------------------------------------------------------------------------------
    
    echo "++++++++++++++++++++++++ STEP 2 +++++++++++++++++++++++++++++++++"
    echo "Backup existing system files beforehand"
    cp -v $sudoers ${sudoers}-bak
    cp -v $passwd ${passwd}-bak
    cp -v $group ${group}-bak
    
    # For eachUser 
    for eachUser in "${userArray1[@]}"
    do
    	# Add them
        useradd $eachUser

        # If user is admin, add a line after vagrant line allowing admin to sudo
    	if [[ "$eachUser" == "admin" ]] ; then
	
		# Would be easier to just add sudo group access for admin	
        	#usermod -G sudo $eachUser
		
		# Check if admin exists in sudoers already
    		isFound=$(grep ^admin $sudoers -c)

		if [[ "$isFound" -eq 0 ]] ; then
			# Add admin in-place in sudoers
            		sed -i '/vagrant ALL=(ALL:ALL) NOPASSWD:ALL/a admin ALL=(ALL:ALL) NOPASSWD:ALL' $sudoers
		else
			# admin already exists in sudoers, don't add
			echo "#### $sudoers already has an entry for $eachUser"
			grep ^${eachUser} $sudoers
		fi
        fi
    done

    echo "####### $passwd ########"
    # Print out results
    for eachUser in "${userArray1[@]}"
    do
	grep ^$eachUser $passwd
    done

    # Verify sudoers updated
    echo "#### Comparison of $sudoers (left) and ${sudoers}-bak (right)"
    diff -y $sudoers ${sudoers}-bak --suppress-common-lines
}

###############################################################################
# Step 3: Create User Group and Collaborative Folder
###############################################################################

# 1. Add an engineers group to the system.
# - Command to add group:

# 2. Add users sam , joe , amy , and sara to the managed group.
# - Command to add users to engineers group (include all four users):

# 3. Create a shared folder for this group at /home/engineers .
# - Command to create the shared folder:

# 4. Change ownership on the new engineers' shared folder to the engineers group.
# - Command to change ownership of engineer's shared folder to engineer group:
###############################################################################
#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# Engineer group name and ID
engGroup="engineers"
engID=4444

# User array
userArray2=( "sam" "joe" "amy" "sara" )

function step_3 () {
    #------------------------------------------------------------------------------

    echo "++++++++++++++++++++++++ STEP 3 +++++++++++++++++++++++++++++++++"
    # Create shared engGroup directory
    mkdir -vp -m 0775 /home/$engGroup

    # Create group
    groupadd -g $engID $engGroup

    # Set ownership of engGroup directory
    chgrp -v $engGroup /home/$engGroup

    # For eachUser add them to $engGroup
    for eachUser in "${userArray2[@]}"
    do
    	# Add each user
        usermod -G $engGroup $eachUser
    done

    ls -ld /home/${engGroup}/

    grep $engGroup $group
}

###############################################################################
# Step 4: Lynis Auditing
###############################################################################

# 1. Command to install Lynis:

# 2. Command to see documentation and instructions:

# 3. Command to run an audit:

# 4. Provide a report from the Lynis output on what can be done to harden the system.
# - Screenshot of report output:
###############################################################################
#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# Program -/some/path/program
program=$(which lynis)
isFound2=$?

# "lynis" not including absolute path
programName=$(echo $program | awk -F'/' '{print $NF}')

function step_4 () {
    #------------------------------------------------------------------------------

    echo "++++++++++++++++++++++++ STEP 4 +++++++++++++++++++++++++++++++++"

    if [[ $isFound2 -eq 0 ]] ; then
   	echo -e "\n$programName is installed\n"

	# Set up log file
	log=${scratch}/${programName}-audit-system.$(date '+%Y%m%d').log

	echo -e "Type \"$programName\" or \"man $programName\" for more information\n"

	# Run system audit
	echo "Running \"lynis audit system --logfile ${log}\"\n"
	lynis audit system --logfile ${log}
	
	echo "lynis audit system --logfile ${log}"

    elif [[ $isFound2 -eq 1 ]] ; then

 	echo -e "\n$programName NOT found\n"

 	echo -e "\nTry installing \"$programName\" with, \"apt-get install $programName\" \n"
        
	# Install app if not found        
        #apt-get install $programName
    fi
}

###############################################################################
# Bonus - chkrootkit
###############################################################################
# 1. Command to install chkrootkit:

# 2. Command to see documentation and instructions:

# 3. Command to run expert mode:

# 4. Provide a report from the chrootkit output on what can be done to harden the system.
# - Screenshot of end of sample output:
###############################################################################
#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# Program -/some/path/program
program1=$(which chkrootkit)
isFound3=$?

# "chkrootkit" not including absolute path
program1Name=$(echo $program1 | awk -F'/' '{print $NF}')

function bonus () {
    #------------------------------------------------------------------------------

    echo "++++++++++++++++++++++++ STEP 4 +++++++++++++++++++++++++++++++++"

    if [[ $isFound3 -eq 0 ]] ; then
   	echo -e "\n$program1Name is installed\n"

	# Set up log file
	log1=${scratch}/${program1Name}-q-quiet-mode.$(date '+%Y%m%d').log

	echo -e "Type \"$program1Name -l\" or \"man $program1Name\" for more information\n"

	# Run system audit
	echo "Running quiet mode and redirecting to log: \"$program1Name -x > ${log}\"\n"
	$program1 -q >> ${log1}
	
	echo "$program1Name quiet mode log file: ${log}"

	# Display the results with INFECTED output
	grep INFECTED $log1

    elif [[ $isFound3 -eq 1 ]] ; then

 	echo -e "\n$program1Name NOT found\n"

 	echo -e "\nTry installing \"$program1Name\" with, \"apt-get install $program1Name\" \n"
        
	# Install app if not found        
        #apt-get install $programName
    fi
}

###############################################################################
# Start program
###############################################################################
# Step 1: Ensure/Double Check Permissions on Sensitive Files
step_1

# Step 2: Create User Accounts
step_2

# Step 3: Create User Group and Collaborative Folder
step_3

# Step 4: Lynis Auditing
step_4

# Bonus - chkrootkit
bonus

###############################################################################
