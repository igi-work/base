#!/bin/bash 
# 
# Week 3 Homework: A High Stakes Investigation
#
# This script contains 4 functions that will complete the 4 objectives of the
# four steps involved.
#
# Step 1 - Investigation Preparation
# Get the files and prepare the environment
#
# Step 2 - Gathering Evidence
# Gather evidence for roulette losses
#
# Step 3 - Correlating the Evidence - Player Analysis
# Correlate players with losses
#
# Step 3 - Correlating the Evidence - Dealer Analysis
# Correlate dealer with losses
#
# Step 4 - Scripting Tasks
#
# Step 5 - Submission
# Produce homework
#
# Variables
###############################################################################
script=$(basename $0)

# Do all work in $scratch
scratch="${HOME}/SCRATCH"

# Base directory of data files
base="${scratch}/Lucky_Duck_Investigations"

# Directory containing Roulette loss data
lossDir="${base}/Roulette_Loss_Investigation"

# Directory containing Player_Analysis files
playerLossDir="${lossDir}/Player_Analysis"

# Player_Analysis notes file
#playerNotesFile="${playerLossDir}/Notes_Player_Analysis.txt"
playerNotesFile="${playerLossDir}/Roulette_Losses.txt"

# Directory containing Dealer_Analysis files
dealerLossDir="${lossDir}/Dealer_Analysis"

# Dealer_Analysis notes file
dealerNotesFile="${dealerLossDir}/Dealers_working_during_losses.txt"
#dealerNotesFile="${dealerLossDir}/Notes_Dealer_Analysis.txt"
#dealerTmpFile="${dealerLossDir}/Notes_Dealer_Analysis.txt-tmp"
dealerTmpFile="${dealerLossDir}/Dealers_working_during_losses.txt-tmp"

# var with file and dir name containing loss correlation
playerDealer="Player_Dealer_Correlation"

# Evidence file
evidence="3-HW-setup-evidence"
###############################################################################

function Step_1 () {
	
	#printf "##################################################\n"
	#printf "Step 1 - Investigation Preparation\n"
	#printf "##################################################\n"

	# Create $base if it doesn't exist
	if [ ! -d $base ] ; then
		mkdir -vp $base
	fi

	# Prep directories
	mkdir -vp ${lossDir}/{Player_Analysis,Dealer_Analysis,Player_Dealer_Correlation}

	# Set working dir
	cd $lossDir 

	# Banner
	printf "\nInvestigation Directory:\n\t$lossDir\n\n" 

	# Array of subdirs in $lossDir
	dir="$(ls $lossDir)"

	# Create a text file in each sub-directory
	for x in `echo $dir`; do 

		printf "BEFORE <<<<<<<<<<<<<<<\n"
		ls -l ${lossDir}/${x}/Notes_${x}.txt 
	
		# Populate notes files in each directory
		echo -e "--------------------------------------------------------------------------"	>> ${lossDir}/${x}/Notes_${x}.txt 
		echo -e "$script Report Created: $(date)" 						>> ${lossDir}/${x}/Notes_${x}.txt 
		echo -e "--------------------------------------------------------------------------\n"	>> ${lossDir}/${x}/Notes_${x}.txt 

		printf "\nAFTER >>>>>>>>>>>>>>> "
		ls -l ${lossDir}/${x}/Notes_${x}.txt 

		printf "\nFile contents:\n"
		# output files
		cat ${lossDir}/${x}/Notes_${x}.txt
	done
	return 123
}

###############################################################################
function Step_2 () {
	#printf "##################################################\n"
	#printf "Step 2 - Gathering Evidence\n"
	#printf "##################################################\n"

	cd $scratch

	# Download and setup evidence script if it doesn't exist
	if [ ! -f "${scratch}/${evidence}" ]; then
		
		# Get evidence file
		wget "https://tinyurl.com/${evidence}"
	
		# Make script executable	
		chmod -v 0700 $evidence
		
		# Execute file
		source $evidence
		
		# Copy date-specific files to final location
		cp -v ${scratch}/Dealer_Schedules_0310/031{0,2,5}*_schedule ${lossDir}/Dealer_Analysis/ 
		cp -v ${scratch}/Roulette_Player_WinLoss_0310/031{0,2,5}*_data ${lossDir}/Player_Analysis/ 
	fi

	return 456
}

###############################################################################
function Step_3_Player_Analysis () {

	#printf "##################################################\n"
	#printf "Step 3 - Correlating the Player_Analysis Evidence\n"
	#printf "##################################################\n"

	cd $playerLossDir

	# Walk through all data files - e.g. 0310_win_loss_player_data
	for  lossFile in $(ls -l *_data | awk '{print $9}') ; do

		# Grab file name and create YYYYMMDD for future conversion - e.g. 20200310
		day=$(echo $lossFile | awk -F'_' '{print "2020"$1}')
	
		# Create file banner	
		echo -e "################################################" 	>> $playerNotesFile
		echo -e "# Day of Loss: $(date -d $day '+%B %d, %Y')"		>> $playerNotesFile
		echo -e "################################################"	>> $playerNotesFile
		
		echo -e "Hour    	Won/Loss	Players"		>> $playerNotesFile
		echo -e "---------------------------------------"		>> $playerNotesFile
		
		# Output all results that contain losses
		cat $lossFile | grep -e '-'					>> $playerNotesFile
	
		# Visual inspection of files lead me to this user	
		suspect="Mylie Schmidt"
		count=$(grep -c "${suspect}" ${lossFile})

		printf "\n${suspect} played ${count} times this day\n\n"	>> $playerNotesFile
	done
}

###############################################################################
function Step_3_Dealer_Analysis () {

	# Start in the Players_Analysis dir
	cd $playerLossDir

	#printf "##################################################\n"
	#printf "Step 3 - Correlating the Dealer_Analysis Evidence\n"
	#printf "##################################################\n"

	# Create file banner	
	printf "#####################################################################################\n"		>> $dealerNotesFile
	printf "# MMDD - Day of Loss	Time of Loss		Roulette Dealer : First and Last Name\n"		>> $dealerNotesFile
	printf "#####################################################################################\n"		>> $dealerNotesFile

	# Walk through all data files that contain losses, e.g. 0310_win_loss_player_data 
	for entry in $(ls -l *_data | awk '{print $9}') ; do

		#file=$entry

		# Grab file name and create YYYYMMDD for future conversion - e.g. 20200310
		day=$(echo $entry | awk -F'_' '{print "2020"$1}')

		# Store the day, e.g. 0310
		dayOfMonth=$(echo ${entry} | awk -F'_' '{print $1}')		

		# Place results in tmp file	
		cat ${dealerLossDir}/${dayOfMonth}_Dealer_schedule | \
			egrep -v '^Hour|^$' | \
			awk -v DOM=$dayOfMonth '{ print DOM " "$1" "$2"\t\t\t"$5" "$6 }'		>> ${dealerTmpFile}-${dayOfMonth}

		# grab only the hours with a loss
		while IFS='' read -r LINE || [ -n "${LINE}" ]; do

			# Get the hour from player loss file
       			hour=$(echo "${LINE}" | awk '{print $1}') 

			# Get the hour from player loss file
       			ampm=$(echo "${LINE}" | awk '{print $2}')    
		
			# Get dealer first name
			#firstName=$(echo "${LINE}" | awk '{print $4}')    
		
			# Get dealer last name
			#lastName=$(echo "${LINE}" | awk '{print $5}')    
	
			# get the hour of loss and dealer involved	
			grep ${hour} ${dealerTmpFile}-${dayOfMonth} | \
			grep ${ampm} | \
			awk '{print $1"			"$2" "$3"			"$4" "$5}'	>> $dealerNotesFile 

		# grab only the hours with a loss
		done < <( grep -e '-' ${playerLossDir}/${dayOfMonth}_win_loss_player_data )

		# Count the number of entries Dealer Name exists
		badDealerCount=$(grep ^$dayOfMonth $dealerNotesFile | awk '{print $4}' | wc -l)

		printf "\nRoulette dealer Billy Jones was involved with ${badDealerCount} losses on ${dayOfMonth}2020 (MMDDYYYY)\n\n"	>> $dealerNotesFile
		printf "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n"			>> $dealerNotesFile
	done

	# Initialize num to 0
	i=0
	#num=0

	#7, 7, 5
	# Player / Employee corellation
	for num in $(grep "times this day" ${playerNotesFile} | awk '{print $4}') ; do
		i=$((i+num)) 
		#echo num: $num
	done

	# Get first and last name of dealer involved
	dealerFirstName=$(grep ^$dayOfMonth $dealerNotesFile | awk '{print $4}' | head -1)
	dealerLastName=$(grep ^$dayOfMonth $dealerNotesFile | awk '{print $5}' | head -1)

	printf "\nConclusion:\n\t${dealerFirstName} ${dealerLastName} was the Roulette dealer every time and day the player, ${suspect}, lost.\n"	>> $dealerNotesFile  
	printf "\t${suspect} lost $i times total. ${suspect} and $dealerFirstName $dealerLastName colluded to scam Lucky Duck.\n"			>> $dealerNotesFile  
}

###############################################################################
#function Step_4_Scripting_Tasks () {
#	#printf "##################################################\n"
#	#printf "Step 4 - Scripting Tasks\n"
#	#printf "##################################################\n"


#}

###############################################################################
function Step_5_Submission () {

	cp -v $dealerNotesFile ${lossDir}/${playerDealer}/
	cp -v $playerNotesFile ${lossDir}/${playerDealer}/
	cp -v ~/bin/${script} ${lossDir}/${playerDealer}/

	cd $lossDir
	zip -r ${playerDealer}.zip ${playerDealer}
}

###############################################################################
function Clean_Up () {

echo -e "##################################################"
echo -e "Clean up Week 3 Homework"
echo -e "##################################################\n"

cd $scratch
rm -rfv 3-HW-setup-evidence Dealer_Schedules_0310/ Lucky_Duck_Investigations/ Roulette_Player_WinLoss_0310/
}

###############################################################################

# RUN PROGRAM

# Step 1 - Investigation Preparation
Step_1

# Step 2 - Gathering Evidence
Step_2

# Step 3a - Correlating the Evidence - Player Analysis
Step_3_Player_Analysis

# Step 3b - Correlating the Evidence - Dealer Analysis
Step_3_Dealer_Analysis

# Step 4 - Scripting Tasks
#Step_4_Scripting_Tasks

# Step 5 - Submission
Step_5_Submission

#Clean_Up

