# Base

Base repository that contains multiple directories named after high-level topics. Within each directory, there are one or more scripts or documents that contain helpful information, links and commands that I've used in the past and that I should document somewhere for future use.

# Background
I keep a long running list of commands, scripts, comments and howtos that I've used in the past. For more strategic work (e.g. arch diagrams, business use cases, data flows), I keep images and more comprehensive documentation in Google Drive along with the draw.io application that's integrated very well with Google Docs.

This way of working might be fast because you're use to searching through a few files and directories, but, it is not optimal in many ways so this 'Base' repo was created to store mostly single-use scripts and commands broken out by topic.

Additionally, this repo will be the fist attempt at working with [Gitbook.com](https://gitbook.com) to create better documentation.

# Linux
 
## Automate ELK Stack
This [repo](https://gitlab.com/igi-work/cs2021) contains the work I created to automate the installation and configuration of an ELK stack deployment on an Ubuntu 18.04 server on the Azure platform.

# Windows 

# Ansible

# Diagrams

# Security

# Misc
